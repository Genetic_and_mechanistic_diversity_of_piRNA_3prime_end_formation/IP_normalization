# Scripts to determine normalization factors for PIWI protein Argonaute IPs
**normalization_factor_finding.sh**

Based on the idea that the combination of IP-data for all expressed PIWI-proteins in a tissue has to reflect the piRNA pool of a total library. Therefore this scripts calculates normalization factors for PIWI-protein IP-libraries that in sum show the best match to a total smallRNA library from the same tissue.

The help-file can be viewed by running the script with option -h.


## Used tools:

fastx-toolkit/0.0.13 

R/2.15.3


optional if you run on cluster

gridengine/2011.11

## Input:
This script requires the following files:
 gzipped fasta-files containing all genome-mapping reads (unique and multi-mappers) in the following format

    \>ID~COUNT

    ID= name of read

    count= sequencing count of the sequence in collapsed files. for non-collapsed files add ~1


## Variables which need to be set by specifying the related flags: 
**total-NAME** -T Name_for_total_library

**total-PATH** -t full_path_to_total_library.fasta

**IP1-NAME** -1 Name_for_Piwi-IP_library

**IP1-PATH** -x full_path_to_Piwi-IP_library.fasta

**IP2-NAME** -2 Name_for_Aub-IP_library

**IP2-PATH** -y full_path_to_Aub-IP_library.fasta

**IP3-NAME** -3 Name_for_AGO3-IP_library

**IP3-PATH** -z full_path_to_AGO3-IP_library.fasta

**total_norm_factor** -N normalization factor for the total-library. calculated by 1000000/sum_of_miRNAs

**COMPUTING**= -C specification if the script should run [default= L]

    locally (L; slow )
    
    cluster (C; uses gridengine; fast; requires the setting of a proper sungrid-header and the change of the host-name in the block marked with #@@@@@@@ in the run_analysis.sh file )
    
    if you are using the cluster, you have to fill in a sungrid-header in the run_R_fit-all-3IPs.sh
              

**FOLDER**  -F Path to permanently store final results

**TMP** -f  Path for the temporary storage of intermediate files


### optinal variables set by specifying the related flags:

**total_CutOff**  -c  cutoff value for the read-count in the total-library [default=5]

**IP_CutOff**  -c  cutoff value for the read-count in the IP-libraries [default=0]

**core-number** -s  number of cores to run prediction in parallel [default: local-processing=10 cluster=150]


## Output

It will generate the following output files:

**normFactors.txt** file containing all the factors determined by fitting. the normalization factor to use is listed under the point "combined-factor"

**protein_distribution.log** rough calculation of the % each PIWI protein contributes to the total smallRNA pool

**final_table.txt** table containing the results of the fitting-process

**IP1_corr_on_flam.txt**  results of the pre-fitting of the Piwi-IP only

**full-datasets/**  folder contains the data-tables used for fitting

