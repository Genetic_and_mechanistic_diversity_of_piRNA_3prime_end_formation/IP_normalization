 
 set -u
###############################################################################
#presetting of required variables 
 

#fastx-toolkit/0.0.13
#R/2.15.3
#gridengine

###############################################################################
 # Argument = -i input -c chunksize -D blast-database -v
usage()

{
  cat << EOF
  usage: $0 options
  
  ###############################################################################
  This script predicts oligopaint probes for the supplied genomic DNA region or RNA sequence.
  Oligos are predicted without spacing on the input DNA, then tested for genome uniqueness
  and then overlapping probes are eliminated.
  
  usage: probe_prediction.sh [options] -i -f -d -b
  
  OPTIONS:
      -h  Show this message
    
 
   ESSENTIAL 
      -f  full-path for temporary folder
            subfolder will be created in this directorh that will get deleted after computing

      -F  full path for Folder to store results
            folder will be created
      
      -T  Name of total library 
            same name as used in the annotation-pipeline            
      -1  Name of IP1 [required] 
            if you want to predict normalization factors for 3 IPs then IP 1 has to be the Piwi IP
            same name as used in the annotation-pipeline            
      -2  Name of IP2 [required]
            same name as used in the annotation-pipeline            
      -3  Name of IP3 [optional - if supplied fitting is done with 3 IPs if empty 2 IP fitting is done]
            same name as used in the annotation-pipeline  
      -N  Normalization factor for the total-library
            Factor calculated by 1000000/sum_of_miRNAs

  optional
      -c  Cuttoff for the total library after normalization to 1M miRNAs
            [default= "5"]
      -C  Cutoff for the IP libraries after normalization to 10M sequenced reads
            [default= "0"]
      -s  number of cores to run the prediction in parallel
            less than 50 is not recommended
            cores are used only for short periods
            [default= "150"]
      -P   local or cluster processing set flag to 
            L local-processing
            C cluster-processing (requires gridengine)
            [default=L]
  
  
EOF
}

tmp_path=
folder_path=
total=
total_path=
IP1=
IP1_path=
IP2=
IP2_path=
IP3=
IP3_path=
total_norm=
total_cutoff="5"
IP_cutoff="0"
splits=
computing="L"

while getopts Òhf:F:T:t:1:x:2:y:3:z:N:c:C:s:L:,Ó OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    f)
      tmp_path=$OPTARG
      ;;
    F)
      folder_path=$OPTARG
      ;;
    T)
      total=$OPTARG
      ;;
    t)
      total_path=$OPTARG
      ;;
    1)
      IP1=$OPTARG
      ;;
    x)
      IP1_path=$OPTARG
      ;;
    2)
      IP2=$OPTARG
      ;;
    y)
      IP2_path=$OPTARG
      ;;
    3)
      IP3=$OPTARG
      ;;
    z)
      IP3_path=$OPTARG
      ;;
    N)
      total_norm=$OPTARG
      ;;
    c)
      total_cutoff=$OPTARG
      ;;
    C)
      IP_cutoff=$OPTARG
      ;;
    s)
      splits=$OPTARG
      ;;
    P)
      computing=$OPTARG
      ;;
    ?)
      usage
      exit
      ;;
  esac
done

##################################################################################################
#test if all obligatory variables are set
if [[ -z $tmp_path ]] || [[ -z $folder_path ]] || [[ -z $total ]] || [[ -z $IP1 ]] || [[ -z $IP2 ]] || [[ -z $IP3 ]]
then
  printf "\n\n not all obligatory variables are set \n\n"
  usage
  exit 1
fi

#set the number of splits depending on the processing environment
if [[ -z $splits ]]
then
  if [[ $computing == C ]]
  then
    splits=150
  else
    splits=10
  fi
fi
  
##################################################################################################

SOURCE="$0"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
script_folder_raw="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
script_folder_raw="${script_folder_raw}/"
script_folder="${script_folder_raw}script-files/"
utility_folder="${script_folder_raw}utility-files/"

#---------------------------------------------------------------------------------------------------------
#create directories and define computing parameters
random=$(echo $RANDOM)
tmp=${tmp_path}/$random/
echo $tmp
mkdir -p $tmp

##---------------------------------------------------------------------------------------------------------
#run the normalization prediction script
output_folder=${folder_path}/${total}-${IP1}-${IP2}-${IP3}-normalization/

rm -rf $output_folder
mkdir -p $output_folder

##################################################################################################
#run analysis

vari="tmp=${tmp},output_folder=${output_folder},script_folder=${script_folder},utility_folder=${utility_folder},total=${total},total_path=${total_path},IP1=${IP1},IP1_path=${IP1_path},IP2=${IP2},IP2_path=${IP2_path},IP3=${IP3},IP3_path=${IP3_path},total_norm=${total_norm},total_cutoff=${total_cutoff},IP_cutoff=${IP_cutoff},splits=${splits},computing=$computing"
${script_folder}3IPs.sh -v "$vari"
   

rm -rf $tmp

echo $output_folder
echo normalization of $IP1 + $IP2 + $IP3 finished

exit


