set -u
  
###################################################################################################

#extract variables if computing on PIWI

vari=

while getopts Ðhv:Ó OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    v)
      vari=$OPTARG
      ;;      
    @)
      usage
      exit
      ;;
  esac
done

host=$(uname -n)
if [[ ! "$host" =~ compute ]]
then
  vari=$(echo $vari | sed 's/,/\t/g;s/"//g' )
  NSLOTS=$
  eval $vari
fi 

###################################################################################################
#create a vector containing all library names
orig_library_names=$(echo ${total} ${IP1} ${IP2} ${IP3} )
library_names=$(echo total IP1 IP2 IP3)

#-------------------------------------------------------------------------------------------------------
#prepare reads of total library for analysis 
#reads get trimmed, length-filtered, collapsed and the counts normalized to 1M miRNAs
module load fastx-toolkit

echo preparing reads for $total
gunzip -c ${total_path} | \
  fastx_trimmer -f 1 -l 23 - | \
  awk '/^>/&&NR>1{print "";}{ printf "%s",/^>/ ? $0" ":$0 }' | \
  awk '{ if ( $1 ~ "@u" || $1 ~ "@m" ) print }' | \
  awk '{ if (length($2) >= 23) print }' | tr '~' ' ' | \
  awk -v total_norm=$total_norm -v tmp=$tmp -v OFS="\t" '{X[$3]+=$2} END{ for (i in X) print i,X[i]*total_norm }' > ${tmp}total-reads-table.txt


#-------------------------------------------------------------------------------------------------------
#read preparation for all IPs
for curr_IP in IP1 IP2 IP3
do 
  varNAME="${curr_IP}_path"
  FILEpath=$(echo ${!varNAME})
  
	#get name of current IP
  library_name=$(echo ${!curr_IP} )
  echo preparing reads of $library_name

  #delete file containing norm-factor
  rm -rf ${tmp}norm.txt
  
  #prepare reads of IP libraries for analysis
	#reads get trimmed, length-filtered and collapsed 
	#in addition the normalization factor to 10M reads that are used in the analysis is calculated
  gunzip -c $FILEpath | \
    fastx_trimmer -f 1 -l 23 - | \
    awk '/^>/&&NR>1{print "";}{ printf "%s",/^>/ ? $0" ":$0 }' | \
    awk '{ if ( $1 ~ "@u" || $1 ~ "@m" ) print }' | \
    awk '{ if (length($2) >= 23) print }' | tr '~' ' ' | \
    awk -v tmp=$tmp -v OFS="\t" '{X[$3]+=$2; Y+=$2} END{NORM=10000000/Y; for (i in X) print i,X[i]; print NORM > tmp"norm.txt" }' > ${tmp}${curr_IP}-reads-table.txt
  
  #extract norm-factor and put into variable	  	
	declare "${curr_IP}_norm=$( cat ${tmp}norm.txt )"
  norm_name=$( echo ${curr_IP}_norm )
  echo norm_factor=${!norm_name}

done

#-------------------------------------------------------------------------------------------------------
#read counts of all libraries get combined into a single table

echo combining reads into single table

i=0; R=${tmp}tmp/
mkdir ${R}

LIBS=$(echo $library_names | wc -w)

for N in `echo $library_names | tr ' ' '\n'`;
do
  echo $N	
  sort -k1,1 ${tmp}${N}-reads-table.txt | awk -vi=$i '{L=$1;for(j=1;j<=i;j++){L=(L" 0")} print L,$2}' > ${R}_$i
  i=$(echo $i | awk '{print $1+1}')
done

printf "seq\ttotal\tIP1\tIP2\tIP3\n" > ${tmp}reads_combined.txt

sort -k1,1 -m ${R}_* | \
awk -v OFS="\t" -vLIBS=$LIBS '{if($1!=o){if(o!=""){L=o;for(i=2;i<=LIBS+1;i++){L=(L" "(N[i]+0))} print L} o=$1; delete N} N[NF]=$NF} 
		END{L=o;for(i=2;i<=LIBS+1;i++){L=(L" "(N[i]+0))} print L}' >> ${tmp}reads_combined.txt

rm -rf ${R}

#-------------------------------------------------------------------------------------------------------
#cutoff factors are getting applied and the ratios for PIWI protein enriched reads is getting calculated
tail -n +2 ${tmp}reads_combined.txt |  \
  awk -v IP_cutoff=$IP_cutoff -v total_cutoff=$total_cutoff -v IP1_norm=$IP1_norm -v IP2_norm=$IP2_norm -v IP3_norm=$IP3_norm -v IP1=$IP1 -v IP2=$IP2 -v IP3=$IP3 -v total=$total '
    BEGIN { print "seq","total","IP1","IP2","IP3","IP1_ratio","IP2_ratio","IP3_ratio"}
    #create normalized read counts for each IP
    {X=$3*IP1_norm; Y=$4*IP2_norm; Z=$5*IP3_norm 
     if(X==0) X=0.1; if(Y==0) Y=0.1; if(Z==0) Z=0.1
    #apply read selection filters and print ratios of normalized read counts
    if ( $2>total_cutoff && $3>IP_cutoff || $2>total_cutoff && $4>IP_cutoff || $2>total_cutoff && $5>IP_cutoff) {
      for (i = NF; i>0; i--)  if ($i == 0) {$i=0.1}
      print $0,X/(Y+Z),Y/(X+Z),Z/(X+Y) }
    }
  ' > ${tmp}reads_combined-ratios.txt

#-------------------------------------------------------------------------------------------------------
#reads get split into PIWI protein biased datasets by requiering >3 fold enrichment
mkdir -p ${output_folder}/full-datasets/

head -n 1 ${tmp}reads_combined-ratios.txt | tr ' ' '\t' | cut -f 1-5 | tee ${output_folder}/full-datasets/IP1-reads.txt ${output_folder}/full-datasets/IP2-reads.txt ${output_folder}/full-datasets/IP3-reads.txt

cat ${tmp}reads_combined-ratios.txt | \
  awk -v OFS="\t" -v folder=${output_folder}full-datasets/ '
    {if(NR>1) {
      if(2>0) {
        if($6 >= 3) {
           print $1,$2,$3,$4,$5 >> folder"IP1-reads.txt"
        }else{
          if($7 >= 3) {
            print $1,$2,$3,$4,$5 >> folder"IP2-reads.txt"
          } else {
            if($8 >=3) 
              print $1,$2,$3,$4,$5 >> folder"IP3-reads.txt"
            }
          }
        } 
      }
    }
  ' 

#####################################################################################################
#prefit IP1 to fix Piwi
##performed to reduce normalization-factor space for the 3-IP fitting

echo fit IP1 only 

cp ${output_folder}/full-datasets/IP1-reads.txt  ${tmp}data.txt
cp ${utility_folder}/factor_list-new-1IP.txt ${tmp}factors.txt
cd $tmp

module load  R/2.15.3
Rscript ${script_folder}fit_IP1_on_flam.R tmp=$tmp

mv ${tmp}IP1_corr_on_flam.txt ${output_folder}IP1_corr_on_flam.txt

###################################################################################################
#run the fitting of the factors

#extract relevant factor combinations based on soma-fitting
optimal_IP1=$(cat ${output_folder}IP1_corr_on_flam.txt | awk '{ if (NR>=2 && NR<=6 )  X+=$1 } END{print X/5}' )
echo IP1 prefitting factor = $optimal_IP1 

cat ${utility_folder}factor_list-new-3IPs.txt | awk -v OFS="\t" -v optimal_IP1=$optimal_IP1 '{ if(NR==1 || $1 >= optimal_IP1*0.33 && $1 <= optimal_IP1*3) print }'  > ${tmp}factor_list.txt
    wc -l ${tmp}factor_list.txt
    

#---------------------------------------------------------------------------------------------------------
#split factor list onto cluster accessible directory
  
echo split factor-list into splits
    
if [[ -z $splits ]]
then
  if [[ $computing == C ]]
  then 
    splits=150
  else
    splits=10
  fi
fi
echo $splits

  
#split factor_list into $splits pieces
#define mode of file splitting (here line by line)
AWKSPLITPRE=""
AWKSPLITVAR="NR"
AWKSPLITNUM="(int(($AWKSPLITVAR-1)*NC/NL)+1)"

header=$(cat ${tmp}factor_list.txt | head -n 1 | tr ' ' '\t' )
cat ${tmp}factor_list.txt | awk '{ if (NR > 1 ) print }'  > ${tmp}factor_list_noheader.txt
		
# count the number of "lines"
LINES=$(cat ${tmp}factor_list_noheader.txt | awk ''$AWKSPLITPRE' END{print '$AWKSPLITVAR'+0}')
echo $LINES

# the number of chunks cannot be greater than the number of "lines"
		if [ $LINES -lt $splits ]; then
		splits=$LINES
 		fi

#split file 
cat ${tmp}factor_list_noheader.txt | awk -vNC=$splits -vNL=$LINES ''$AWKSPLITPRE' {print > ("'${tmp}'/"'$AWKSPLITNUM'"x.txt")}'

mkdir -p ${tmp}splits
for i in $(seq 1 $splits)
do 
  echo $header | tr ' ' '\t'  > ${tmp}splits/${i}.txt
  cat ${tmp}${i}x.txt | tr ' ' '\t'  >> ${tmp}splits/${i}.txt
  rm -rf ${tmp}${i}x.txt
done

#move all the data into cluster-accessible directory
cluster_tmp=${tmp}cluster/
rm -rf ${cluster_tmp}
mkdir -p $cluster_tmp


cp ${tmp}splits/* ${cluster_tmp}

rm -rf ${tmp}factor_list_noheader.txt

#---------------------------------------------------------------------------------------------------------
# generate data table and store on cluster accessible directory

available_categories=$( printf "IP1\tIP2\tIP3" )

for curr_IP in IP1 IP2 IP3
do 
   
  cp ${output_folder}/full-datasets/${curr_IP}-reads.txt ${cluster_tmp}data.txt
  
  cd ${cluster_tmp}
  command=${script_folder}run_R_fit-all-3IPs.sh
  vari="cluster_tmp=${cluster_tmp},script_folder=${script_folder},utility_folder=${utility_folder}"
  if [[ $computing == C ]]
  then
    qsub -sync Y -t 1:$splits -v "${vari}" $command
  else
    for n in $(seq 1 $splits)
    do
      vari="cluster_tmp=${cluster_tmp},script_folder=${script_folder},utility_folder=${utility_folder},SGE_TASK_ID=${n}"
      $command -v $vari &
    done
    wait
  fi
    
  #---------------------------------------------------------------------------------------------------------
  #combine output of junks into 1 file
  
  echo combine output
  
  cat ${cluster_tmp}1_processed.txt > ${tmp}processed.txt
  for i in $(seq 2 $splits)
  do
  	cat ${cluster_tmp}${i}_processed.txt | awk '{if (NR >1) print }' >> ${tmp}processed.txt
  done
  
  #---------------------------------------------------------------------------------------------------------

  mkdir -p ${tmp}/factor_finding_lists/ 
  cat ${tmp}processed.txt > ${tmp}/factor_finding_lists/${curr_IP}.txt
  
done

#---------------------------------------------------------------------------------------------------------
#extract the resutls from the individual datasets 
sort -k1,1n -k2,2n -k3,3n ${tmp}/factor_finding_lists/IP1.txt | awk '{ if (NR>1) print $1"~"$2"~"$3,$6}' >${tmp}IP1.txt
sort -k1,1n -k2,2n -k3,3n ${tmp}/factor_finding_lists/IP2.txt | awk '{ if (NR>1) print $1"~"$2"~"$3,$6}' >${tmp}IP2.txt
sort -k1,1n -k2,2n -k3,3n ${tmp}/factor_finding_lists/IP3.txt | awk '{ if (NR>1) print $1"~"$2"~"$3,$6}' >${tmp}IP3.txt

printf "factor-combination\tIP1\tIP2\tIP3\tdiff-of-sum-ratios\n" > ${output_folder}final_table.txt
join ${tmp}IP1.txt  ${tmp}IP2.txt | join -  ${tmp}IP3.txt | \
  awk -v OFS="\t" '{ print $0,$2+$3+$4 }' | \
  tr ' ' '\t' | sort -k5,5n | head -n 2000 >> ${output_folder}final_table.txt
     


#####################################################################################################
#extract final IP normalization factors and report various statistics        NR 
IP1_norm_final=$(cat ${output_folder}final_table.txt | awk '{ if (NR >= 2 && NR <=6 ) {split ($1,norm_factors,/~/);  X+=norm_factors[1] }}END{print X/5}' )
IP2_norm_final=$(cat ${output_folder}final_table.txt | awk '{ if (NR >= 2 && NR <=6 ) {split ($1,norm_factors,/~/);  X+=norm_factors[2] }}END{print X/5}' )
IP3_norm_final=$(cat ${output_folder}final_table.txt | awk '{ if (NR >= 2 && NR <=6 ) {split ($1,norm_factors,/~/);  X+=norm_factors[3] }}END{print X/5}' )

printf "IP_normalization_factor= ${IP1_norm_final}\tTotal=${total}\tIP1=${IP1}\tIP2=${IP2}\tIP3=${IP3}\n" >> ${output_folder}/${IP1}/IP_normalization_factor.txt
printf "IP_normalization_factor= ${IP2_norm_final}\tTotal=${total}\tIP1=${IP1}\tIP2=${IP2}\tIP3=${IP3}\n" >> ${output_folder}/${IP2}/IP_normalization_factor.txt
printf "IP_normalization_factor= ${IP3_norm_final}\tTotal=${total}\tIP1=${IP1}\tIP2=${IP2}\tIP3=${IP3}\n" >> ${output_folder}/${IP3}/IP_normalization_factor.txt


#---------------------------------------------------------------------------------------------------------
#extrapolate biological factor 
IP1_BioNorm=$( echo $IP1_norm_final | awk -v NORM=$IP1_norm '{print $1*NORM}' )
IP2_BioNorm=$( echo $IP2_norm_final | awk -v NORM=$IP2_norm '{print $1*NORM}' )
IP3_BioNorm=$( echo $IP3_norm_final | awk -v NORM=$IP3_norm '{print $1*NORM}' )

echo total library > ${output_folder}normFactors.txt
echo normalization_to_1M_miRNAs $total_norm $total >> ${output_folder}normFactors.txt
printf "\n" >> ${output_folder}normFactors.txt
echo $IP1 >> ${output_folder}normFactors.txt
echo sequencing_depth_normalization $IP1_norm  >> ${output_folder}normFactors.txt
echo biological_abundance_normalization $IP1_BioNorm  >> ${output_folder}normFactors.txt
echo combined_factor $IP1_norm_final  >> ${output_folder}normFactors.txt
printf "\n" >> ${output_folder}normFactors.txt
echo $IP2 >> ${output_folder}normFactors.txt
echo sequencing_depth_normalization $IP2_norm  >> ${output_folder}normFactors.txt
echo biological_abundance_normalization $IP2_BioNorm  >> ${output_folder}normFactors.txt
echo combined_factor $IP2_norm_final  >> ${output_folder}normFactors.txt
printf "\n" >> ${output_folder}normFactors.txt
echo $IP3 >> ${output_folder}normFactors.txt
echo sequencing_depth_normalization $IP3_norm  >> ${output_folder}normFactors.txt
echo biological_abundance_normalization $IP3_BioNorm  >> ${output_folder}normFactors.txt
echo combined_factor $IP3_norm_final  >> ${output_folder}normFactors.txt

#---------------------------------------------------------------------------------------------------------
#calculate read distribution in libraries

echo $IP1_norm $IP2_norm $IP3_norm $IP1_norm_final $IP2_norm_final $IP3_norm_final $IP1 $IP2 $IP3 | \
  awk '{ IP1=10000000/$1/$4; IP2=10000000/$2/$5; IP3=10000000/$3/$6; total=IP1+IP2+IP3;
    IP1x=IP1*100/total; IP2x=IP2*100/total; IP3x=IP3*100/total;
    print $7"="IP1x"%\n"$8"="IP2x"%\n"$9"="IP3x"%" }' > ${output_folder}protein_distribution.log


rm -rf $tmp
rm -rf $cluster_tmp

exit
