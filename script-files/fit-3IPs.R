
#---------------------------------------------------------------------------------------------------------------------


args  =  commandArgs(TRUE);
argmat  =  sapply(strsplit(args, "="), identity)

for (i in seq.int(length=ncol(argmat))) {
  assign(argmat[1, i], argmat[2, i])
}

# available variables
print(ls())

#---------------------------------------------------------------------------------------------------------------------
setwd(cluster_tmp)
print(cluster_tmp)

#read in table containing the counts for the tiles
input_table = read.table("data.txt",header=T)

#read in factor list
factor_list_name = paste (split_nr,".txt",sep="")
factor_list = read.table(factor_list_name,header=T)

#correct the counts of the total library by the supplied correction value
t=input_table[,1:5]

#get colnumbers for various columns
col_raw_IP1 = grep("IP1", colnames(input_table),fixed=TRUE)
col_factor_IP1=grep("IP1", colnames(factor_list),fixed=TRUE)
col_raw_IP2 = grep("IP2", colnames(input_table),fixed=TRUE)
col_factor_IP2=grep("IP2", colnames(factor_list),fixed=TRUE)
col_raw_IP3 = grep("IP3", colnames(input_table),fixed=TRUE)
col_factor_IP3=grep("IP3", colnames(factor_list),fixed=TRUE)

#---------------------------------------------------------------------------------------------------------------------


#create calculation fields
factor_list[6:7,"sum_total"] =  NA
col_sum_total=grep("sum_total", colnames(factor_list),fixed=TRUE)

factor_list[,col_sum_total]  = sum(t$total)

factor_list[6:7,"sum_IPs"] =  NA
factor_list[6:7,"diff_to_1"] =  NA

#create function for the calculation
input1=t[,col_raw_IP1]
input2=t[,col_raw_IP2]
input3=t[,col_raw_IP3]
calculate_sum = function(x,y,z){
  a = sum(input1/x,input2/y,input3/z)
  return(a)
}

factor1=factor_list[,col_factor_IP1]
factor2=factor_list[,col_factor_IP2]
factor3=factor_list[,col_factor_IP3]

ptm <- proc.time()
factor_list$sum_IPs = mapply(calculate_sum,factor1,factor2,factor3)
#print time consumption
proc.time() - ptm

factor_list$diff_to_1 = abs (1 - (factor_list$sum_total  / factor_list$sum_IPs ))

#sort the table according the stdev and output the head of the table
processed_factor_list_name = paste (split_nr,"_processed.txt",sep="")
write.table(factor_list,file=processed_factor_list_name, sep="\t", row.names = FALSE)

