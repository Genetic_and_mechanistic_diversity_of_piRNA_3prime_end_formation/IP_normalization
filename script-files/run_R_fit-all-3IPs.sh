#$ -S /bin/sh

#put sungrid-header

 
 set -u
  
###################################################################################################

#extract variables if computing on PIWI

vari=

while getopts �hv:� OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    v)
      vari=$OPTARG
      ;;      
    @)
      usage
      exit
      ;;
  esac
done

host=$(uname -n)
if [[ ! "$host" =~ compute ]]
then
  vari=$(echo $vari | sed 's/,/\t/g;s/"//g' )
  NSLOTS=1
  eval $vari
echo a
fi 


###################################################################################################
#go to local directory
cd $cluster_tmp

module load  R/2.15.3
Rscript ${script_folder}fit-3IPs.R cluster_tmp=${cluster_tmp} split_nr=${SGE_TASK_ID} 

exit
